package main

import (
	"flag"
	db "go-temp/internal/pkg/db"
	"go-temp/internal/routers"
	"go-temp/pkg/util/logger"
)

// func init() {
// 	logger.LogInit()
// }

var (
	BsName = flag.String("module_name", "TEMP-GO", "this module name")
)

func main() {
	logger.InitLogger(*BsName)
	defer db.Setup()

	router := routers.InitApiRouter()
	_ = router.Run("0.0.0.0:8888")

}

package redis

import (
	"flag"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"go-temp/pkg/util/logger"

	// "github.com/sirupsen/logrus"
	"time"
)

var (
	redisUrl    = flag.String("redis_url", "127.0.0.1:6379", "redis database address")
	redisPasswd = flag.String("redis_passwd", "", "redis database address")
	client      *redis.Client
)

const (
	Nil = redis.Nil

	defaultRedisKeyExpire = 2 * time.Hour
)

func newClient() *redis.Client {
	if client != nil {
		return client
	}
	client = redis.NewClient(&redis.Options{
		Addr:     *redisUrl,
		Password: *redisPasswd, // 默认空密码
		DB:       0,            // 使用默认数据库
	})
	logger.Logger.Info("redis_url : ", *redisUrl)
	return client
}

func Hset(cname string, data *map[string]interface{}) (s string, err error) {
	s, err = newClient().HMSet(cname, *data).Result()
	if err != nil {
		logger.Logger.Info("redis字典[%s]保存失败", cname)
		err = errors.Wrap(err, fmt.Sprintf("redis字典[%s]保存失败", cname))
	}
	_, _ = Expire(cname, defaultRedisKeyExpire)
	logger.Logger.Debugf("redis字典[%s]保存%v", cname, *data)
	return
}

func Hget(cname string) (data map[string]string, err error) {
	data, err = newClient().HGetAll(cname).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis字典[%s]查询失败", cname))
	}
	logger.Logger.Infof("redis字典[%s]查询结果%v", cname, data)
	return data, err
}
func HsetOne(cname, key, value string) (bool, error) {
	b, err := newClient().HSet(cname, key, value).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis字典[%s]添加[%s:%s]失败", cname, key, value))
	}
	_, _ = Expire(cname, defaultRedisKeyExpire)
	logger.Logger.Debugf("redis字典[%s]添加[%s:%s]", cname, key, value)
	return b, err
}
func HgetOne(cname, key string) (string, error) {
	s, err := newClient().HGet(cname, key).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis字典[%s]查询[%s]失败", cname, key))
	}
	logger.Logger.Debugf("redis字典[%s]查询[%s]结果[%s]", cname, key, s)
	return s, err
}

func Get(cname string) (string, error) {
	s, err := newClient().Get(cname).Result()
	if err == redis.Nil {
		err = nil
	}
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis查询[%s]失败", cname))
	}

	logger.Logger.Debugf("redis查询[%s]结果[%s]", cname, s)
	return s, err
}

func SetNx(cname string, value interface{}) (bool, error) {
	return newClient().SetNX(cname, value, defaultRedisKeyExpire).Result()
}
func Set(cname string, value interface{}) (string, error) {
	return newClient().Set(cname, value, defaultRedisKeyExpire).Result()
}
func SetEx(cname string, value interface{}, expiration time.Duration) (string, error) {
	return newClient().Set(cname, value, expiration).Result()
}

func SSetNx(cname, value string) (bool, error) {
	return newClient().SetNX(cname, value, defaultRedisKeyExpire).Result()
}
func SSet(cname, value string) (string, error) {
	return newClient().Set(cname, value, defaultRedisKeyExpire).Result()
}
func SSetEx(cname, value string, expiration time.Duration) (string, error) {
	return newClient().Set(cname, value, expiration).Result()
}

func Incr(cname string) (i int64, err error) {
	i, err = newClient().Incr(cname).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis自增[%s]失败", cname))
		return
	}
	logger.Logger.Debugf("redis自增[%s]结果[%s]", cname, i)
	return
}
func Zero(cname string) (s string, err error) {
	s, err = newClient().Set(cname, 0, 0).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis保存[%s]结果[%s]失败", cname, s))
		return
	}
	_, _ = Expire(cname, defaultRedisKeyExpire)
	logger.Logger.Debugf("redis保存[%s]结果[%s]", cname, s)
	return

}

func List(cname, order string, skip, limit int64) ([]string, error) {
	if order == "" {
		order = "ASC"
	}
	return newClient().Sort(cname, &redis.Sort{Offset: skip, Count: limit, Order: order}).Result()
}
func Lset(cname string, index int64, data interface{}) (s string, err error) {
	s, err = newClient().LSet(cname, index, data).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis列表[%s]按index位置[%d]设置[%v]失败", cname, index, data))
		logrus.Infoln(err.Error())
		return
	}

	logger.Logger.Debugf("redis列表[%s]按index位置[%d]添加[%v]", cname, index, data)
	return
}
func LPush(cname string, data *[]interface{}) (i int64, err error) {
	i, err = newClient().LPush(cname, *data...).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis列表[%s]左添加[%v]失败", cname, *data))
		logrus.Infoln(err.Error())
		return
	}
	logger.Logger.Debugf("redis列表[%s]左添加[%v]", cname, *data)
	return
}
func RPush(cname string, data *[]interface{}) (i int64, err error) {
	i, err = newClient().RPush(cname, *data...).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis列表[%s]右添加[%v]失败", cname, *data))
		logrus.Infoln(err.Error())
		return
	}
	_, _ = Expire(cname, defaultRedisKeyExpire)
	logger.Logger.Debugf("redis列表[%s]右添加[%v]", cname, *data)
	return
}
func LLen(cname string) (i int64, err error) {
	i, err = newClient().LLen(cname).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis查询列表[%s]长度失败", cname))
		return
	}
	logger.Logger.Debugf("redis列表[%s]长度[%v]", cname, i)
	return
}
func LRange(cname string, start, stop int64) (data []string, err error) {
	data, err = newClient().LRange(cname, start, stop).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis查询列表[%s][%v~%v]失败", cname, start, stop))
		return
	}
	logger.Logger.Debugf("redis列表[%s][%v~%v][%v]", cname, start, stop, data)
	return
}
func LRangeAll(cname string) (data []string, err error) {
	total, err := newClient().LLen(cname).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis查询列表[%s]失败", cname))
		return
	}
	data, err = newClient().LRange(cname, 0, total).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis查询列表[%s]失败", cname))
		return
	}
	logger.Logger.Debugf("redis列表[%s]全部数据[%v]", cname, data)
	return
}
func Del(cname string) (i int64, err error) {
	i, err = newClient().Del(cname).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis删除[%s]失败", cname))
		return
	}
	logger.Logger.Debugf("redis删除[%s]", cname)
	return
}

/*
 设置失效时间
*/
func Expire(cname string, expire time.Duration) (i bool, err error) {
	i, err = newClient().Expire(cname, expire).Result()
	if err != nil {
		err = errors.Wrap(err, fmt.Sprintf("redis设置失效时间[%s]失败", cname))
	}
	return
}

package db

import (
	"flag"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"go-temp/pkg/util/logger"

	"time"
)

var db *gorm.DB

type Model struct {
	ID         int `gorm:"primary_key" json:"id"`
	CreatedOn  int `json:"created_on"`
	ModifiedOn int `json:"modified_on"`
	DeletedOn  int `json:"deleted_on"`
}

var (
	username = flag.String("db_username", "xuh", "mysql database name")      //账号
	password = flag.String("db_password", "root", "mysql database password") //密码
	host     = flag.String("db_host", "127.0.0.1", "mysql database host")    //数据库地址，可以是Ip或者域名
	port     = flag.Int("db_port", 3306, "mysql database port")              //数据库端口
	Dbname   = flag.String("db_name", "go-temp", "mysql database name")

	//	残疾人mini数据库
	// username = flag.String("db_username", "robot", "mysql database name")               //账号
	// password = flag.String("db_password", "Fitme.ai.ro", "mysql database password") //密码
	// host     = flag.String("db_host", "10.10.1.22", "mysql database host")              //数据库地址，可以是Ip或者域名
	// port     = flag.Int("db_port", 3306, "mysql database port")                        //数据库端口
	// Dbname   = flag.String("db_name", "phoneminideafmutecallcenter", "mysql database name")             //数据库名

	timeout = "10s" //连接超时，10秒s

	// Db *gorm.DB
)

func Setup() {
	var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local&timeout=%s", *username, *password, *host, *port, *Dbname, timeout)

	printDsn := fmt.Sprintf("charset=utf8&parseTime=True&loc=Local&timeout=%s", timeout)
	logger.Logger.Info("建立mysql数据库连接: " + printDsn)
	db, err = gorm.Open("mysql", dsn)

	if err != nil {
		logger.Logger.Fatal("models.Setup err: %v", err)
	}

	// gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
	// 	return setting.DatabaseSetting.TablePrefix + defaultTableName
	// }

	db.SingularTable(true)
	db.Callback().Create().Replace("gorm:update_time_stamp", updateTimeStampForCreateCallback)
	db.Callback().Update().Replace("gorm:update_time_stamp", updateTimeStampForUpdateCallback)
	db.Callback().Delete().Replace("gorm:delete", deleteCallback)
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)

	// //自动检查所有表
	// checkTable(db)
}

func CloseDB() {
	defer db.Close()
}

// updateTimeStampForCreateCallback will set `CreatedOn`, `ModifiedOn` when creating
func updateTimeStampForCreateCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		nowTime := time.Now().Unix()
		if createTimeField, ok := scope.FieldByName("CreatedOn"); ok {
			if createTimeField.IsBlank {
				_ = createTimeField.Set(nowTime)
			}
		}

		if modifyTimeField, ok := scope.FieldByName("ModifiedOn"); ok {
			if modifyTimeField.IsBlank {
				_ = modifyTimeField.Set(nowTime)
			}
		}
	}
}

// updateTimeStampForUpdateCallback will set `ModifiedOn` when updating
func updateTimeStampForUpdateCallback(scope *gorm.Scope) {
	if _, ok := scope.Get("gorm:update_column"); !ok {
		_ = scope.SetColumn("ModifiedOn", time.Now().Unix())
	}
}

func deleteCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		var extraOption string
		if str, ok := scope.Get("gorm:delete_option"); ok {
			extraOption = fmt.Sprint(str)
		}

		deletedOnField, hasDeletedOnField := scope.FieldByName("DeletedOn")

		if !scope.Search.Unscoped && hasDeletedOnField {
			scope.Raw(fmt.Sprintf(
				"UPDATE %v SET %v=%v%v%v",
				scope.QuotedTableName(),
				scope.Quote(deletedOnField.DBName),
				scope.AddToVars(time.Now().Unix()),
				addExtraSpaceIfExist(scope.CombinedConditionSql()),
				addExtraSpaceIfExist(extraOption),
			)).Exec()
		} else {
			scope.Raw(fmt.Sprintf(
				"DELETE FROM %v%v%v",
				scope.QuotedTableName(),
				addExtraSpaceIfExist(scope.CombinedConditionSql()),
				addExtraSpaceIfExist(extraOption),
			)).Exec()
		}
	}
}

func addExtraSpaceIfExist(str string) string {
	if str != "" {
		return " " + str
	}
	return ""
}

package routers

import (
	"github.com/gin-gonic/gin"
	v1 "go-temp/internal/api/v1"
)

func InitApiRouter() *gin.Engine {
	var router *gin.Engine

	router = gin.Default()

	// 处理静态资源（不建议gin框架处理静态资源，参见 Public/readme.md 说明 ）
	router.Static("/static", "./static") //  定义静态资源路由与实际目录映射关系
	// router.StaticFile("/abcd", "./public/readme.md") // 可以根据文件名绑定需要返回的文件名

	//  创建一个门户类接口路由组
	group := router.Group("/api/v1/")
	{
		// 模拟一个首页路由
		vApi := group.Group("home/")
		{
			// 第二个参数说明：
			// 1.它是一个表单参数验证器函数代码段，该函数从容器中解析，整个代码段略显复杂，但是对于使用者，您只需要了解用法即可，使用很简单，看下面 ↓↓↓
			// 2.编写该接口的验证器，位置：app/http/validator/api/home/news.go
			// 3.将以上验证器注册在容器：app/http/validator/common/register_validator/register_validator.go
			// 46行为注册时的键（consts.ValidatorPrefix + "HomeNews"）。那么获取的时候就用该键即可从容器获取
			vApi.GET("news", v1.FindUserController)
		}
	}

	return router
}

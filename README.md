# go-template

## /cmd

该项目的主程序.

每个程序目录的名字应该和可执行文件的名字保持一致 (比如 `/cmd/myapp`).

不要在程序目录中放太多代码. 如果你觉得这些代码会被其他项目引用, 那它们应该被放在 `/pkg` 目录中. 如果这些代码不能被重用, 或者说你不希望别人重用这些代码, 那么就把它们放在 `/internal` 目录中. 你也许会惊讶于别人使用你代码的方式, 所以一定要保持你的意图足够明确!

一般来说一个足够小的 `main` 函数, 用于引入并执行 `/internal` 和 `/pkg` 下的代码就足够了.

例子请参考 [`/cmd`](https://github.com/golang-standards/project-layout/blob/master/cmd/README.md) 目录

## /internal

程序和库的私有代码. 这里的代码都是你不希望被别的应用和库所引用的.

把你真正的应用代码放在 `/internal/app` 目录 (比如: `/internal/app/myapp`) 把你的应用间共享的代码放在 `/internal/pkg` 目录 (比如: `internal/pkg/myprivlib`)

## /pkg

可以被其他外部应用引用的代码 (比如: `/pkg/mypubliclib`). 其他项目会引入这些库并期望它们能正常工作, 所以把代码放在这里之前还请三思 :-)

如果你的代码仓库根目录中包含很多非 Go 的组件和目录, 那么把 Go 代码组织到同一个目录下也算是一种方式, 这么做可以让你更轻松地使用不少 Go 工具 (GopherCon EU 2018 里的 [`Best Practices for Industrial Programming`](https://www.youtube.com/watch?v=PTE4VJIdHPg) 有提过这部分的内容 ).

如果你想看看有哪些热门项目使用了这种项目结构可以参考 [`/pkg`](https://github.com/golang-standards/project-layout/blob/master/pkg/README.md) 目录. 这是一种常见的布局模式, 但它并没有被完全接受, 有些 Go 社区并不推荐使用.

## /vendor

应用的依赖 (手工管理或者是通过你最爱的依赖管理工具像是 [`dep`](https://github.com/golang/dep)).

如果你在构建一个库项目, 那注意不要把依赖也提交上去了.

作者：GotaX

链接：https://www.jianshu.com/p/4726b9ac5fb1

来源：简书

著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

## /api

OpenAPI/Swagger 规范, JSON schema 文件, 协议定义文件.

例子请参考 [`/api`](https://github.com/golang-standards/project-layout/blob/master/api/README.md)

# 常规应用目录

## /configs

配置文件模板或者默认配置.

在这里放置你的 `confd` 或者 `consul-template` 模板文件.

## /init

系统初始化 (systemd, upstart, sysv) 及进程管理/监控 (runit, supervisord) 配置.

## /scripts

执行各种构建, 安装, 分析等其他操作的脚本.

这些脚本要保持根级别的 Makefile 小而简单 (比如: <https://github.com/hashicorp/terraform/blob/master/Makefile>).

例子请参考 [`/scripts`](https://github.com/golang-standards/project-layout/blob/master/scripts/README.md) 目录

## /build

打包及持续集成.

将 cloud (AMI), container (Docker), OS (deb, rpm, pkg) 包配置放在 `/build/package` 目录下.

将 CI (travis, circle, drone) 配置和脚本放在 `/build/ci` 目录. 需要注意的是, 有些 CI 工具 (比如 Travis CI) 对它们配置文件的位置非常挑剔. 尝试把配置文件放在 `/build/ci` 目录, 然后将它们软链接到 CI 工具希望它们出现的位置 (如果可能的话).

## /deployments

IaaS, Paas, 系统, 容器编排的部署配置和模板 (docker-compose, kubernetes/helm, mesos, terraform, bosh)

## /test

额外的外部测试软件和测试数据. 你可以用任意方式自由地组织 `/test` 目录的结构. 大型项目则有必要包含一个 data 子目录. 比如你可以建立 `/test/data` 或者 `/test/testdata` 目录, 如果你希望 Go 忽略里面的内容. 注意, Go 也会忽略任何以 "." 和 "_" 开头的文件和目录, 所以就如何命名测试数据目录而言, 你拥有更多的灵活性.

作者：GotaX

链接：https://www.jianshu.com/p/4726b9ac5fb1

来源：简书

著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

## /docs

用户及设计文档 (除了 godc 生成的文档).

例子请参考  [`/docs`](https://github.com/golang-standards/project-layout/blob/master/docs/README.md) 目录.

## /tools

项目的支持工具. 注意, 这些工具可以引入 `/pkg` 和 `/internal` 目录的代码.

例子请参考 [`/tools`](https://github.com/golang-standards/project-layout/blob/master/tools/README.md) 目录.

## /examples

应用或者库的示例文件.

例子请参考 [`/examples`](https://github.com/golang-standards/project-layout/blob/master/examples/README.md) 目录.

## /third_party

外部辅助工具, forked 代码, 以及其他第三方工具 (例如: Swagger UI)

作者：GotaX

链接：https://www.jianshu.com/p/4726b9ac5fb1

来源：简书

著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。